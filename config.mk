_VERSION = 0.5
VERSION  = `git describe --tags --dirty 2>/dev/null || echo $(_VERSION)`
CC = cc -std=c11

PKG_CONFIG = pkg-config

# paths
PREFIX = /usr/local
MANDIR = $(PREFIX)/share/man
DATADIR = $(PREFIX)/share

# Building Xwayland support
XWAYLAND = -DXWAYLAND
XLIBS = xcb xcb-icccm xcb-composite xcb-ewmh xcb-render xcb-res xcb-xfixes

# To build wlroots statically linked
# It assumes you have the wlroots project inside this direcory and you built the static library
WLROOTS_LDFLAG = -lm -L./wlroots/build -l:libwlroots.a
WLROOTS_PKGS = libseat pixman-1 libudev libdisplay-info gbm libdrm vulkan egl gl wayland-client
WLROOTS_CFLAGS = -I./wlroots/include

# If you prefer to link dynamically comment the 3 lines above and uncomment the following ones
# WLROOTS_CFLAGS =
# WLROOTS_LDFLAG =
# WLROOTS_PKGS = wlroots
